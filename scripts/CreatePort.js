//loading styles from CSS
const form = document.querySelector('form');


//using addEventListner() method to record the data
form.addEventListener('submit', function (e) 
{
    e.preventDefault();
    
    //getting form data
    const name = document.getElementById("name").value;
    const country = document.getElementById("country").value;
    const type = document.getElementById("type").value;
    const size = document.getElementById("size").value;
    const location = document.getElementById("location").value;
    var lat ;
    var lng ;
    
    //    Make the request to get API data using a callback function
    portsResponse = function( portList )
        {

            lat = portList.results[0].geometry.lat;
            lng = portList.results[0].geometry.lng;
            //create new port instance
            let port = new Port(name, country, type, size, lat, lng)
            // Saving to local storage
            var existingPorts = JSON.parse(localStorage.getItem("allPorts"));
            if(existingPorts == null) existingPorts = new PortList();
            existingPorts.ports.push(port)
            localStorage.setItem("allPorts", JSON.stringify(existingPorts)); 
        
            //Redirect to 
            window.location.href="index.html";
        }

     
        var data = {
            q:`${location}`,
            key:"8739b49f320e4b609834b335790271ba",
            language:'en',
            pretty:'1',
            callback: "portsResponse"
        };
        jsonpRequest("https://api.opencagedata.com/geocode/v1/json", data);
        function jsonpRequest(url, data)
        {
            // Build URL parameters from data object.
            var params = "";
            // For each key in data object...
            for (var key in data)
            {
                if (data.hasOwnProperty(key))
                {
                    if (params.length == 0)
                        {
                            // First parameter starts with '?'
                        params += "?";
                        }
                else
                        {
                            // Subsequent parameter separated by '&'
                            params += "&";
                        }
                var encodedKey = encodeURIComponent(key);
                var encodedValue = encodeURIComponent(data[key]);
                params += encodedKey + "=" +encodedValue;

                }
            }
        var script = document.createElement('script');
        script.src = url + params;
        document.body.appendChild(script);
        }

  
});
  
  
  
  
  
  
  
  