let container = document.getElementById('container');
let portMaker = (port) => 
{
    let ul = document.createElement('ul');
    ul.setAttribute('class', 'ul-port-card');

    ul.innerHTML = `
    ${ port.name }
    <li class="li-port-detail">
        ${ port.country }
    </li>
    <li class="li-port-detail">
        ${ port.type }
    </li>
    <li class="li-port-detail">
        ${ port.size }
    </li>
    <li class="li-port-detail">
        ${ port.lat }
    </li>
    <li class="li-port-detail">
        ${ port.lng }
    </li>
  `;
    container.appendChild(ul);
}


window.onload = function() 
{
    var ports;
    //get from local storage
    var ports_obj = JSON.parse(localStorage.getItem('allPorts'));
    //if port list is not in local storage, create an empty array
    if(ports_obj == null) 
    {
        ports= []
    } 
    else 
    {
        ports = ports_obj.ports;
    }
    
        //    Make the request
    var data = {
        callback: "this.portsResponse"
    };
        
    jsonpRequest("https://eng1003.monash/api/v1/ports/", data);
        
    function jsonpRequest(url, data)
    {
    // Build URL parameters from data object.
    var params = "";
    // For each key in data object...
        for (var key in data)
        {
            if (data.hasOwnProperty(key))
                {
                if (params.length == 0)
                {
                // First parameter starts with '?'
                params += "?";
                }
                else
                {
                // Subsequent parameter separated by '&'
                params += "&";
                }
                var encodedKey = encodeURIComponent(key);
                var encodedValue = encodeURIComponent(data[key]);
                params += encodedKey + "=" +encodedValue;
                }
        }
            
    var script = document.createElement('script');
    script.src = url + params;
    document.body.appendChild(script);
    
    }



        this.portsResponse = function(portList)
        {
            portList.ports.forEach((port) => {
                ports.push(port)
            })
            ports.forEach(port => {
                portMaker(port)
            })
        }

}



