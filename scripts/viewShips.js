const container = document.getElementById('container');
const shipMaker = (ship) => {
    const ul = document.createElement('ul');
    ul.setAttribute('class', 'ul-ship-card');

    ul.innerHTML = `
    ${ ship.name }
    <li class="li-ship-detail">
        Ship name: ${ ship.name }
    </li>
    <li class="li-ship-detail">
        Max_speed: ${ ship.maxSpeed }
    </li>
    <li class="li-ship-detail">
        Range :${ ship.range }
    </li>
    <li class="li-ship-detail">
        Description: ${ ship.desc }
    </li>
    <li class="li-ship-detail">
        Cost per Km: ${ ship.cost }
    </li>
    <li class="li-ship-detail">
        Status: ${ ship.status }
    </li>
  `;
    container.appendChild(ul);
  }


window.onload = function() {
    var ships;
    //get from local storage
    var ships_obj = JSON.parse(localStorage.getItem('allShips'));
    //if ship list is not in local storage, create an empty array
    if(ships_obj == null) {
        ships= []
    } else {
        ships = JSON.parse(localStorage.getItem('allShips')).ships;
    }
    //    Make the request
    var data = {
        callback: "this.shipsResponse"
    };
        jsonpRequest("https://eng1003.monash/api/v1/ships/", data);
        function jsonpRequest(url, data)
        {
            // Build URL parameters from data object.
            var params = "";
            // For each key in data object...
            for (var key in data)
            {
            if (data.hasOwnProperty(key))
            {
            if (params.length == 0)
            {
            // First parameter starts with '?'
            params += "?";
            }
            else
            {
            // Subsequent parameter separated by '&'
            params += "&";
            }
            var encodedKey = encodeURIComponent(key);
            var encodedValue = encodeURIComponent(data[key]);
            params += encodedKey + "=" +encodedValue;

            }
            }
            var script = document.createElement('script');
            script.src = url + params;
            document.body.appendChild(script);
        }



        this.shipsResponse = function(shipList)
        {
            shipList.ships.forEach((ship) => {
                ships.push(ship)
            })
            ships.forEach(ship => {
                shipMaker(ship)
            })
        }

    }

